import {
  SET_NEWS,
  ERRORS_IN_NEWS,
  FINISH_LOAD_NEWS,
  DELETE_HACKER_NEWS,
} from '../types/HackerNewsTypes';

const initialState = {
  news: [],
  loading: true,
  error: false,
};

const hackerNewsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_NEWS:
      return { ...state, news: action.news };
    case ERRORS_IN_NEWS:
      return { ...state, loading: false, error: true };
    case FINISH_LOAD_NEWS:
      return { ...state, loading: false };
    case DELETE_HACKER_NEWS:
      return {
        ...state,
        news: state.news.filter((hackerNew) => hackerNew !== action.hackerNew),
      };
    default:
      return state;
  }
};

export default hackerNewsReducer;
