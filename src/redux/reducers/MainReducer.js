import { combineReducers } from 'redux';
import hackerNewsReducer from './HackerNewsReducer';

const mainReducer = combineReducers({
  hackerNewsReducer,
});

export default mainReducer;
