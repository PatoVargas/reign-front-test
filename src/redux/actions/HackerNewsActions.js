import {
  SET_NEWS,
  ERRORS_IN_NEWS,
  FINISH_LOAD_NEWS,
  DELETE_HACKER_NEWS,
} from '../types/HackerNewsTypes';

import {
  callGetHackerNews,
  callDeleteNew,
} from '../../services/HackerNewsServices';

export const setNews = (news) => ({
  type: SET_NEWS,
  news,
});

export const setNewsLoaded = () => ({
  type: FINISH_LOAD_NEWS,
});

export const setNewsError = () => ({
  type: ERRORS_IN_NEWS,
});

export const deleteNew = (hackerNew) => ({
  type: DELETE_HACKER_NEWS,
  hackerNew,
});

export const getHackerNews = () => async (dispatch) => {
  const response = await callGetHackerNews();
  if (response.status === 200) {
    dispatch(setNewsLoaded());
    dispatch(setNews(response.data.news));
  } else dispatch(setNewsError());
};

export const deleteHackerNew = (hackerNew) => async (dispatch) => {
  const response = await callDeleteNew(hackerNew);
  if (response.status === 200) {
    dispatch(deleteNew(hackerNew));
    return true;
  }
  return false;
};
