import axios from 'axios';

const axiosInstance = axios.create({
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
  },
});

const onResponseSuccess = (response) => response;

const onResponseFail = (error) => () => error.response;

axiosInstance.interceptors.response.use(onResponseSuccess, onResponseFail);

export default axiosInstance;
