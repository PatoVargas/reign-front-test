import axiosInstance from '../providers/Axios';

import { newsUrl } from '../config/paths';

export const callGetHackerNews = async () => axiosInstance({
  url: newsUrl,
  method: 'get',
});

export const callDeleteNew = async (data) => axiosInstance({
  url: newsUrl,
  method: 'delete',
  data,
});

export default callGetHackerNews;
