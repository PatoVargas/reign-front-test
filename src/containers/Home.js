import React from 'react';

import Box from '@material-ui/core/Box';

import Welcome from '../components/Home/Welcome';
import NewList from '../components/Home/NewList';

function Home() {
  return (
    <Box>
      <Welcome />
      <NewList />
    </Box>
  );
}

export default Home;
