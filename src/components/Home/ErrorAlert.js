import React from 'react';
import Alert from '@material-ui/lab/Alert';

function ErrorAlert() {
  return (
    <Alert severity="error" style={{ marginTop: 10 }}>
      Error in delete hacker news, try again later :c
    </Alert>
  );
}

export default ErrorAlert;
