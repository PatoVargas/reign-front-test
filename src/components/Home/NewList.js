import React from 'react';
import { useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';

import HackerNew from './HackerNew';

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  containerLoading: {
    textAlign: 'center',
  },
}));

function NewList() {
  const classes = useStyles();

  const loading = useSelector((state) => state.hackerNewsReducer.loading);
  const news = useSelector((state) => state.hackerNewsReducer.news);
  const error = useSelector((state) => state.hackerNewsReducer.error);

  return (
    <Container className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          {loading && (
            <div className={classes.containerLoading}>
              <CircularProgress color="secondary" />
            </div>
          )}
          {!loading && !error && (
            <>
              {news.map((value) => {
                if (value.story_title || value.title) {
                  return (
                    <HackerNew
                      key={`${value.story_id}-${value.parent_id}-${value.author}`}
                      hackerNew={value}
                    />
                  );
                }
                return <></>;
              })}
            </>
          )}
        </Grid>
      </Grid>
    </Container>
  );
}

export default NewList;
