import React from 'react';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import styled from '@emotion/styled';
import { FaTrashAlt } from 'react-icons/fa';

import { deleteHackerNew } from '../../redux/actions/HackerNewsActions';

import AlertDelete from './AlertDelete';
import ErrorAlert from './ErrorAlert';

const NewItem = styled.div`
  padding: 20px;
  border-bottom: solid;
  border-color: #ccc;
  border-width: 1px;
  font-size: 13pt;
  background-color: #fff;
  &:hover {
    background-color: #fafafa;
    .containerTrash{
      display: block !important;
    }
  }
`;

const TitleOrTimeText = styled.div`
  display: inline;
  margin-right: 5px;
  color: #333;
`;

const AuthorText = styled.div`
  display: inline;
  color: #999;
`;

const useStyles = makeStyles(() => ({
  centerColumn: {
    textAlign: 'center',
    margin: 'auto',
  },
}));

function HackerNew({ ...props }) {
  const classes = useStyles();
  const { hackerNew } = props;
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [errorDelete, setErrorDelete] = React.useState(false);
  const reduxDispatch = useDispatch();

  const handleClickOpen = () => {
    setErrorDelete(false);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const openHackerNew = () => {
    const url = hackerNew.story_url || hackerNew.url;
    // eslint-disable-next-line no-undef
    window.open(url, '_blank');
  };

  const deleteHackerNewClick = async () => {
    setLoading(true);
    const response = await reduxDispatch(
      deleteHackerNew(hackerNew),
    );
    if (!response) setErrorDelete(true);
    handleClose();
    setLoading(false);
  };

  return (
    <NewItem>
      <AlertDelete
        open={open}
        handleClose={handleClose}
        deleteHackerNew={deleteHackerNewClick}
        loading={loading}
      />
      <Grid container spacing={3}>
        <Grid
          item
          xs={9}
          onClick={openHackerNew}
        >
          <TitleOrTimeText>
            {hackerNew.story_title || hackerNew.title}
          </TitleOrTimeText>
          <AuthorText>
            -
            {' '}
            {hackerNew.author}
            {' '}
            -
          </AuthorText>
        </Grid>
        <Grid
          item
          xs={2}
          className={classes.centerColumn}
          onClick={openHackerNew}
        >
          <TitleOrTimeText>
            {moment(hackerNew.created_at).format('DD-MM-YYYY')}
          </TitleOrTimeText>
        </Grid>
        <Grid
          item
          xs={1}
          className={classes.centerColumn}
          onClick={handleClickOpen}
        >
          <div className="containerTrash" style={{ display: 'none' }}>
            <FaTrashAlt />
          </div>
        </Grid>
      </Grid>
      { errorDelete && <ErrorAlert /> }
    </NewItem>
  );
}

export default HackerNew;
