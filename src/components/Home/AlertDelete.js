import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';

function AlertDelete({ ...props }) {
  const {
    open,
    handleClose,
    deleteHackerNew,
    loading,
  } = props;

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">¿Are you sure?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            You will delete a news item permanently, you have no turning back.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" disabled={loading}>
            Not, cancel!
          </Button>
          <Button
            onClick={deleteHackerNew}
            color="primary"
            disabled={loading}
            autoFocus
          >
            {!loading ? (
              <>Yes, sure!</>
            ) : (
              <CircularProgress color="secondary" />
            )}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AlertDelete;
