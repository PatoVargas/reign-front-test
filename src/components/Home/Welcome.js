import React from 'react';
import Box from '@material-ui/core/Box';
import styled from '@emotion/styled';

const PageTitle = styled.h1({
  fontSize: 60,
  margin: 0,
  paddingBottom: 10,
});

const PageSubtitle = styled.h3({
  margin: 0,
  fontSize: 30,
});

const boxStyle = {
  backgroundColor: '#333333',
  padding: 60,
};

function Welcome() {
  return (
    <Box color="white" style={boxStyle}>
      <PageTitle>HN Feed</PageTitle>
      <PageSubtitle>We &#60;3 hacker news!</PageSubtitle>
    </Box>
  );
}

export default Welcome;
