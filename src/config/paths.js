const baseUrl = process.env.REACT_APP_URL_BASE_BACKEND;

export const newsUrl = `${baseUrl}/news`;

export default baseUrl;
