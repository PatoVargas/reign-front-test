# REIGN TEST FRONT

This is React app created to a technical test in reign software factory. To run this project make sure you have docker and docker compose installed. 

## How run

Create a .env file in root dir project with the next vars 

```
REACT_APP_URL_BASE_BACKEND=http://localhost:8080
```
### Docker

```
docker-compose build
docker-compose up 
```

### Without docker

```
npm install
npm start
```